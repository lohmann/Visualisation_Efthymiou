#rowscale
#
#This functions scale the rows of the heatmap, this is a pheatmap package function
#
# @param x matrix to scale by rows
#You can learn more about pheatmap package at:
#
#   https://cran.r-project.org/web/packages/pheatmap/pheatmap.pdf
rowscale<-function (x)
{
  m = apply(x, 1, mean, na.rm = T)
  s = apply(x, 1, sd, na.rm = T)
  return((x - m)/s)
}


# plot_list_color
# Heatmap of TRUE / FALSE
#
# @param TABLE a TRUE / FALSE row
# @param ColonneGene ColonneGene
# @export
# @import ggplot2
# @importFrom stats sd
#
plot_list_color<-function(TABLE, ColonneGene){
  
  ggplot(TABLE, aes(y=Name, x=get(ColonneGene),fill=Metabo))+
    geom_tile()+
    theme(axis.line=element_blank(),
          axis.text.x=element_blank(),
          axis.ticks.x = element_blank(),
          axis.title.x=element_blank(),
          axis.title.y=element_blank(),
          panel.background=element_blank(),
          panel.border=element_blank(),
          panel.grid.major=element_blank(),
          panel.grid.minor=element_blank(),
          plot.background=element_blank())
  
  
}

#scale_tbl
# Table and Scale
#
# @param table TMM table
# @param listeGenes List of genes
# @param ColonneGene The column name of the gene symbol
#
# @return a scaled table
# @export
# @importFrom  reshape melt
# @import tibble
# @import dplyr
#
# scale_tbl<-function(table,
#                     listeGenes,
#                     ColonneGene){
#   out=list()
#   out[["TMM"]]=melt(table[which(table[[ColonneGene]]%in%listeGenes),])
#   out[["scaled"]]=table[which(table[[ColonneGene]]%in%listeGenes),]%>%distinct(get(ColonneGene), .keep_all = TRUE) %>%
#     remove_rownames%>%
#     column_to_rownames(ColonneGene)%>% select(!`get(ColonneGene)`)%>%
#     rowscale()%>%
#     rownames_to_column(ColonneGene)%>%
#     melt()#%>%rename(scaled="value")
#   colnames(out[["scaled"]])<-c(ColonneGene,c("variable","scaled"))
#   return(out)
# }



scale_tbl<-function(table,
                    listeGenes,
                    ColonneGene){
  out=list()
  
  if(!listeGenes%in%table[[ColonneGene]]%>%all()){
    genes=listeGenes[which(!listeGenes%in%table[[ColonneGene]])]
    t=data.frame(genes,rep(NA,length(genes)),rep(NA,length(genes)),rep(NA,length(genes)),rep(NA,length(genes)),rep(NA,length(genes)),rep(NA,length(genes))) %>% mutate(across(.cols=c(2:7), .fns=as.numeric))
    colnames(t)=colnames(table[which(table[[ColonneGene]]%in%listeGenes),])        
    Newtable=rbind(table[which(table[[ColonneGene]]%in%listeGenes),],t)
  }else{
    Newtable=table[which(table[[ColonneGene]]%in%listeGenes),]
  }
  
  out[["TMM"]]=melt(Newtable)
  out[["scaled"]]=Newtable%>%distinct(get(ColonneGene), .keep_all = TRUE) %>%
    remove_rownames%>%
    column_to_rownames(ColonneGene)%>% select(!`get(ColonneGene)`)%>%
    rowscale()%>%
    rownames_to_column(ColonneGene)%>%
    melt()#%>%rename(scaled="value")
  colnames(out[["scaled"]])<-c(ColonneGene,c("variable","scaled"))
  return(out)
}
# plot_heat
# Red and Blue heatmap
#
# @param table merged scaled and normal TMM table
# @param titre Title
# @param ColonneGene The column name of the gene symbol
#
# @return a scaled table
# @export
# @import ggplot2
#
plot_heat <- function(table,
                      ColonneGene,
                      titre,
                      size=textsize){
  
  ggplot(table, aes(variable, get(ColonneGene),  fill= scaled)) +
    geom_tile() +
    scale_fill_gradient2(high="red",mid="white",low="blue",
                         na.value="white", midpoint=mean(table$scaled, na.rm=T)
    ) +
    geom_text( aes(label = ifelse(is.na(value), "", round(value, 1))),size=size) +
    theme_minimal() +
    labs(title=titre)+
    theme( axis.text.x = element_text(angle = 90, vjust = 0.5)
           ,legend.position="none",axis.title.x=element_blank(),
           axis.title.y=element_blank(),axis.text.y=element_blank(),
           axis.ticks = element_blank())
  
}

# tbl_tmm_pval
# TMM and pvalue
#
# @param table table
# @param columns columns
# @param geneName geneName
# @param listG listG
#
# @return Values
# @export
# @importFrom  reshape melt
# @importFrom tidyr drop_na

tbl_tmm_pval<-function(table,
                       columns,
                       geneName,
                       listG){
  rbind(table[which(table[[geneName]]%in%listG),columns],
        {if (!identical(listG[!listG%in%table[[geneName]]], character(0)) )
          as.data.frame(list(listG[!listG%in%table[[geneName]]], NA,NA) , col.names=	columns)
          else NA }) %>%
    as.data.frame()%>%melt()%>%drop_na(1)-> Values
  return(Values)
  
}


# plot_add_heat
# Small heat
#
# @param table table
# @param geneName geneName
#
# @return Values
# @export
# @import ggplot2
#
plot_add_heat<-function(table,
                        geneName, 
                        size=textsize){
  ggplot(table, aes(y=get(geneName), x=variable,fill=value))+ geom_tile() +
    scale_fill_gradient2(high="orange",mid="white",low="cyan",
                         na.value="white", midpoint=0
    )+
    theme(axis.line=element_blank(),
          axis.text.x = element_text(angle = 90, vjust = 0.5),
          axis.text.y=element_blank(),
          axis.ticks = element_blank(),
          axis.title.x=element_blank(),
          axis.title.y=element_blank(),
          legend.position="none",
          panel.background=element_blank(),
          panel.border=element_blank(),
          panel.grid.major=element_blank(),
          panel.grid.minor=element_blank(),
          plot.background=element_blank())+
    geom_text( aes(label = ifelse(is.na(value), "", round(value, 3))),size=size)
  
}


#plot_merged_heat
#
# @param GSEA1 GSEA1
# @param GSEA2 GSEA2
# @param GSEA_name1 GSEA_name1
# @param GSEA_name2 GSEA_name2
# @param TMM1 TMM1
# @param TMM2 TMM2
# @param DE1 DE1
# @param DE2 DE2
# @param colnames1 colnames1
# @param colnames2 colnames2
# @param ColonneGene1 ColonneGene1
# @param ColonneGene2 ColonneGene2
# @param name1 name1
# @param name2 name2
# @param method method intersect ou union
# @param Metabolist Metabolic list
#
# @return Values
# @export
# @import ggplot2
# @import ggpubr
# @importFrom stats setNames
# @import purrr
# @import grid
#
plot_merged_heat<-function(TMM1,
                           DE1,
                           colnames1,
                           ColonneGene1,
                           TMM2,
                           DE2,
                           colnames2,
                           ColonneGene2,
                           name1,
                           name2
                           ,Metabolist
                           ,listG
                           ,el="Comparision"
                           ,axissize
                           ,textsize
                           
){
  
  
  if (identical(listG, character(0))){
    invisible()
  }else{
    
    ISmetabo<-as.data.frame(list(
      listG
      ,listG%in%Metabolist
      ,rep("gene",length(listG)))
      ,col.names=c("Name","Metabo","gene")
    )
    
    metabo_part<-plot_list_color(ISmetabo , "gene")
    legend <- get_legend(metabo_part)
    out=list()
    out[[name1]]=scale_tbl(table=TMM1, listeGenes=listG,ColonneGene=ColonneGene1)
    out[[name2]]=scale_tbl(table=TMM2, listeGenes=listG,ColonneGene=ColonneGene2)
    
    a=plot_heat(table=merge(out[[name1]]$TMM,out[[name1]]$scaled),
                ColonneGene=ColonneGene1,
                titre=name1
                ,size=textsize)
    
    b=plot_heat(table=merge(out[[name2]]$TMM,out[[name2]]$scaled),
                ColonneGene=ColonneGene2,
                titre=name2
                ,size=textsize)
    
    
    tmm=list()
    tmm[[name1]]=tbl_tmm_pval(table=DE1,columns=colnames1,geneName=ColonneGene1,listG=listG)
    tmm[[name2]]=tbl_tmm_pval(table=DE2,columns=colnames2,geneName=ColonneGene2,listG=listG)
    
    heat_tmm=list()
    
    heat_tmm[[name1]]=plot_add_heat(table=tmm[[name1]],geneName=ColonneGene1,size=textsize)
    heat_tmm[[name2]]=plot_add_heat(table=tmm[[name2]],geneName=ColonneGene2,size=textsize)
    
    
    # theme(
    #   panel.border = element_blank(), panel.grid.major = element_blank(),
    #   panel.grid.minor = element_blank(),
    #   axis.text.x = element_text(angle = input$num, vjust = 0.5),
    #   ,legend.text = element_text(size = input$legendsize)
    #   ,legend.title= element_text(size = input$legendsize)
    #   ,axis.text=element_text(size = input$axissize)
    #   ,axis.title=element_text(size = input$namesaxis)
    #   ,title=element_text(size = input$titlesize)
    # )
    
    
    figure <-ggarrange( metabo_part+theme( legend.position="none" ,axis.text=element_text(size = axissize) ),
                        a ,
                        heat_tmm[[name1]],
                        b ,
                        heat_tmm[[name2]],
                        as_ggplot(legend), NULL,NULL,NULL,NULL,
                        widths =c(0.3,1,0.3,1,0.3),heights = c(1,0.05),
                        ncol = 5, nrow = 2,align = "h"
                        ,labels = NULL
    )
    
    
    
    cat(sprintf("\n
### %s
\n", gsub('_',' ',el) ) )
    
    
    FIG<-annotate_figure(figure, left = textGrob("Gene Symbol", rot = 90, vjust = 1, gp = gpar(cex = 1.3)),
                         bottom = textGrob("Samples", gp = gpar(cex = 1.3)),
                         top = text_grob(gsub('_',' ',el),
                                         color = "red", face = "bold", size = 14))
    
    
    
    return(list("figure"=FIG,"annotation"=ISmetabo,"parameters"=tmm,"values"=out))
  }
  
  
  
  
}


prettyDT <- function(x,...){
  datatable(
    x,
    #selection = "none", #none, mutilple
    rownames = FALSE,
    # extensions = c("ColReorder","Buttons"),#,"Select"),
    
    # callback = JS(js),
    
    class = 'cell-border stripe',
    filter="top",...
  )
}



